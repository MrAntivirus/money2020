var express = require('express');
var app = express();
var request = require('request');

// Configure
app.configure(function() {
	app.use(express.bodyParser());
	app.use(express.json());
	app.use(express.urlencoded());
	app.use(express.methodOverride());
	
	app.use(express.static('www'));
});


// Backend routes
app.get('/menu', function(res,req){
	req.sendfile('./data/menu.json');
});

function sleep(time, callback) {
    var stop = new Date().getTime();
    while(new Date().getTime() < stop + time) {
        ;
    }
    callback();
}

app.post('/order', function(res,req){
	// success path
	
	var options = {
		url: 'https://w1.mercurycert.net/PaymentsAPI/Credit/Sale',
		headers: {
			'User-Agent': 'request',
			'Content-Type': 'application/json',
			'Authorization': 'Basic MDAzNTAzOTAyOTEzMTA1Onh5eg=='
		},
		form: res.body
	};
	
	// Do post to Mercury
	request.post(options, function(err, response, body){
		if(!err && response.statusCode == 200){
			sleep(0, function() {
				// executes after one second, and blocks the thread
				req.send(body);
			});
		}
		else {
			req.send(500);
		}
	});
});

// Used to emulate a bad transaction
app.post('/order/fail', function(res,req){
	// fail path
	req.sendfile('./data/failureResult.json');
});

// MUST BE LAST!
app.get('*', function(req,res){
	res.sendfile('www/index.html');
});

// Listen on the default.
app.listen(80);