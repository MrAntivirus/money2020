angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $http, $stateParams, jsonFactory) {
	$scope.OrderedItems = [];
	$scope.Seats = [
			{SeatNum:1},
			{SeatNum:2},
			{SeatNum:3},
			{SeatNum:4},
			{SeatNum:5},
			{SeatNum:6},
			{SeatNum:7},
			{SeatNum:8},
		];
	
	$scope.GetMenu = function(){
		$scope.Menu = jsonFactory.Data;
	};
	
	$scope.ProcessingPayment = false;
	
	$scope.GetMenu();
	
	function QueryStringToJSON(queryString) {            
		var pairs = queryString.slice(1).split('&');
		
		var result = {};
		pairs.forEach(function(pair) {
			pair = pair.split('=');
			result[pair[0]] = decodeURIComponent(pair[1] || '');
		});

		return JSON.parse(JSON.stringify(result));
	};
	
	$scope.PostOrder = function(seat){
		var data = {
			InvoiceNo:"1",
			RefNo:"1",
			Memo:"Table" + $scope.TableNum + "-Seat" + seat.Seat,
			Purchase:$scope.TotalBill,
			AccountSource:"Swiped",
			AcctNo:"5499990123456781",
			ExpDate:"0816",
			OperatorID:"Aria Cafe"
		};
		$scope.ProcessingPayment = true;
		
		$http.post('/order', data).success(function(data){
			// if data.CmdStatus == "Approved", do something
			var dataInJSON = QueryStringToJSON(data);
			if (dataInJSON.CmdStatus == "Approved") {
				seat.SeatBill = 0.00;
				seat.BillBGColor = 'lightgreen';
				//$stateParams.state('tab.success');
			}
			
			$scope.ProcessingPayment = false;
		})
		.error(function(err){
			// Do something on failure.
			$scope.ProcessingPayment = false;
		});
	};
	
	$scope.AddItemToMenu = function(menuItem, Seat) {
		var item = {
			name: menuItem.name,
			price: menuItem.price
		};
		
		var found = false;
		for (var i=0; i<$scope.OrderedItems.length; ++i) {
			console.log($scope.OrderedItems[i].Seat);
			console.log(Seat.SeatNum);
			if ($scope.OrderedItems[i].Seat == Seat.SeatNum) {
				$scope.OrderedItems[i].Items.push(item);
				$scope.OrderedItems[i].SeatBill = $scope.CalculateSeatBill($scope.OrderedItems[i].Items);
				found = true;
				console.log($scope.CalculateSeatBill(Seat.SeatNum));
				break;
			}
		}
		
		if( !found ) {
			$scope.OrderedItems.push({
				Items:[item],
				Seat:Seat.SeatNum,
				SeatBill: $scope.CalculateSeatBill([item]),
				BillBGColor: '#ff0000'
			});
			console.log($scope.CalculateSeatBill(Seat.SeatNum));
		}
		
		//$scope.OrderedItems.sort(function(a,b){ return (a.seat - b.seat);});
		$scope.SelectedMenuItem = null;
		$scope.TotalBill = $scope.CalculateTotalBill();
	};
	
	$scope.CalculateSeatBill = function(itemList){
		var total = 0;
		
		for(var i = 0; i < itemList.length; ++i) {
			total += parseFloat(itemList[i].price);
		}
	
		return total;
	};
	
	$scope.CalculateTotalBill = function(){
		var total = 0;
		
		angular.forEach($scope.OrderedItems, function(value, key) {
			if ($scope.OrderedItems[key] != undefined) {
				for(var j = 0; j < $scope.OrderedItems[key].Items.length; ++j) {
					total += parseFloat($scope.OrderedItems[key].Items[j].price);
				}
			}
		});
		
		return total;
	};
	
	$scope.RemoveItem = function(seat, index){
		console.log(index);
		seat.Items.splice(index,1);
		console.log(seat.Items);
		$scope.TotalBill = $scope.CalculateTotalBill();
		seat.SeatBill = $scope.CalculateSeatBill(seat.Items);
		
		if (seat.Items.length == 0) {
			for (var i=0; i < $scope.OrderedItems.length; ++i) {
				if ($scope.OrderedItems[i] === seat) {
					$scope.OrderedItems.splice(i,1);
					break;
				}
			}
		}
	};
})

.controller('FriendsCtrl', function($scope, Friends) {
  $scope.friends = Friends.all();
})

.controller('FriendDetailCtrl', function($scope, $stateParams, Friends) {
  $scope.friend = Friends.get($stateParams.friendId);
})

.controller('AccountCtrl', function($scope) {
})

.factory('jsonFactory', function() {
	var jsonFac = {
		Data: [
			{
			"_id": "54557e949cc295ac781749d6",
			"name": "Veal",
			"price": "8.26"
			},
			{
			"_id": "54557e94f4b1bc5c50685ff8",
			"name": "Cheese Volcano",
			"price": "6.61"
			},
			{
			"_id": "54557e9459a6fc9beaafb1d8",
			"name": "Baked Potato",
			"price": "2.53"
			},
			{
			"_id": "54557e94cf0e4834cf242d36",
			"name": "Jumbo Bacon Buger",
			"price": "8.51"
			},
			{
			"_id": "54557e94ef424ee75acd1084",
			"name": "Chip 'n fish",
			"price": "7.88"
			},
			{
			"_id": "54557e940e04ce557d2434df",
			"name": "El Beefo",
			"price": "16.29"
			}
		]
	};
	
	return jsonFac;
});

